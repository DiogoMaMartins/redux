import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Provider } from 'react-redux';
import store from './store';
import Todolist from './Todolist.js';
import Counter from './Counter.js';
import Counters from './Counters.js';

export default class App extends Component  {
  state = {
    counter:0
  }

  increment = () => {
    this.setState({
      counter: this.state.counter += 1
    })
  }

  decrement = () => {
    this.setState({
      counter: this.state.counter -= 1
    })
  }

  render(){
    return(
      <Provider store={store}>
        <div>
          <Todolist/>
          <Counter/>
          <Counters/>
        </div>
      </Provider>
    );
  }
}
