export default function counter(state= 0, action) {
  switch (action.type) {
    case 'increment':
      return action.value +=1
    case 'decrement':
      return action.value -=1
    default:
      return state;

  }
}
