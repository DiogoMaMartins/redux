export function increment(value) {
  return {
    type:'increment',
    value,
  }
}

export function decrement(value) {
  return {
    type:'decrement',
    value,
  }
}
