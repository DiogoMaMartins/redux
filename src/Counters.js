import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as counterActions from './actions/counter';

class Counters extends Component {
  state = {
    newValue:2,
  }

  increment = () => {
    this.props.increment(this.props.counter);
  }
  decrement = () => {
    this.props.decrement(this.props.counter);
  }
  render(){
    return(
      <div>
        <h1>{this.props.counter}</h1>
        <button onClick={this.increment}><p>increment</p></button>
          <button onClick={this.decrement}><p>decrement</p></button>
      </div>
    );
  }
}

const  mapStateToProps = state => ({
  counter:state.counter,
});

const mapDispatchToProps = dispatch => bindActionCreators(counterActions, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(Counters);
